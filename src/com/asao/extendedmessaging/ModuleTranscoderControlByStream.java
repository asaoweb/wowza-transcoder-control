package com.asao.extendedmessaging;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.wowza.wms.amf.AMFPacket;
import com.wowza.wms.application.*;
import com.wowza.wms.media.model.MediaCodecInfoAudio;
import com.wowza.wms.media.model.MediaCodecInfoVideo;
import com.wowza.wms.medialist.MediaList;
import com.wowza.wms.medialist.MediaListRendition;
import com.wowza.wms.medialist.MediaListSegment;
import com.wowza.wms.module.*;
import com.wowza.wms.rest.vhosts.VHostConfig;
import com.wowza.wms.stream.*;
import com.wowza.wms.stream.livetranscoder.ILiveStreamTranscoder;
import com.wowza.wms.stream.livetranscoder.ILiveStreamTranscoderControl;
import com.wowza.wms.stream.livetranscoder.ILiveStreamTranscoderNotify;
import com.wowza.wms.stream.livetranscoder.LiveStreamTranscoderItem;
import com.wowza.wms.transcoder.model.ILiveStreamTranscoderActionNotify;
import com.wowza.wms.transcoder.model.LiveStreamTranscoder;
import com.wowza.wms.transcoder.model.TranscoderSessionAudioEncode;
import com.wowza.wms.transcoder.model.TranscoderSessionDataEncode;
import com.wowza.wms.transcoder.model.TranscoderSessionDestination;
import com.wowza.wms.transcoder.model.TranscoderSessionVideoEncode;
import com.wowza.wms.transcoder.model.TranscoderStreamNameGroup;

/**
 * For performance reason, it is much better to do not transcode a stream to a better resolution than the source stream.
 * When the engine get the first Video codec information of any transcoded stream, it will check if the resolution of the transcoded
 * stream is better than the source stream and if it's the case, the stream is shutdown.
 * Only the first video codec received for the original stream is taken in account (the source webcam is initialized to it's best resolution).
 * So the source streams will be transcoded only in resolutions smaller than the best quality stream of the source webcam.
 *
 */
public class ModuleTranscoderControlByStream extends ModuleBase {
	
	/**
	 * We keep a list of all available ILiveStreamTranscoder instances for the application instance, in order to be able
	 * to find the source stream of a transcoded stream.
	 */
	class TranscoderListener implements ILiveStreamTranscoderNotify {

		@Override
		public void onLiveStreamTranscoderCreate(ILiveStreamTranscoder liveStreamTranscoder, IMediaStream stream) {
			getLogger().info("Transcoder created : " + liveStreamTranscoder.getTranscoderName() + " ; src : " + liveStreamTranscoder.getStreamName() + " ; stream " + stream.getName());
			((LiveStreamTranscoder)liveStreamTranscoder).addActionListener(new TranscoderActionListener());
		}

		@Override
		public void onLiveStreamTranscoderDestroy(ILiveStreamTranscoder liveStreamTranscoder, IMediaStream stream) {
		}

		@Override
		public void onLiveStreamTranscoderInit(ILiveStreamTranscoder liveStreamTranscoder, IMediaStream stream) {

			WMSProperties appProps = liveStreamTranscoder.getAppInstance().getProperties();
			Set<ILiveStreamTranscoder> transcoders = (Set<ILiveStreamTranscoder>)(appProps.getProperty("transcoders"));
			if (transcoders == null) {
				synchronized (appProps)
				{
					transcoders = new HashSet<>();
					appProps.put("transcoders", transcoders);
				}
			}
			transcoders.add(liveStreamTranscoder);
		}
		
	}
	
	class TranscoderActionListener implements ILiveStreamTranscoderActionNotify {

		/**
		 * We're saving the calculated audio rate in the transcoder, in order to have it for the generation of the SMIL file
		 * @param liveStreamTranscoder
		 * @param bitrate
		 */
		@Override
		public void onCalculateSourceAudioBitrate(LiveStreamTranscoder liveStreamTranscoder, long bitrate) {
			getLogger().info("Source audio birate " + liveStreamTranscoder.getTranscoderName() + " : " + bitrate);
			EncodeConfiguration sourceEncode = EncodeConfigurationBuilder.getInstance().getEncodeConfigurationByName("source");
			if (sourceEncode != null) {
				sourceEncode.setBitrateAudio(bitrate);
			}
		}

		/**
		 * We're saving the calculated video rate in the transcoder, in order to have it for the generation of the SMIL file
		 * @param liveStreamTranscoder
		 * @param bitrate
		 */
		@Override
		public void onCalculateSourceVideoBitrate(LiveStreamTranscoder liveStreamTranscoder, long bitrate) {
			getLogger().info("Source video birate " + liveStreamTranscoder.getTranscoderName() + " : " + bitrate);
			EncodeConfiguration sourceEncode = EncodeConfigurationBuilder.getInstance().getEncodeConfigurationByName("source");
			if (sourceEncode != null) {
				sourceEncode.setBitrateVideo(bitrate);
			}
		}

		@Override
		public void onInitAfterLoadTemplate(LiveStreamTranscoder liveStreamTranscoder) {
			
		}

		@Override
		public void onInitBeforeLoadTemplate(LiveStreamTranscoder liveStreamTranscoder) {
			
		}

		@Override
		public void onInitStart(LiveStreamTranscoder liveStreamTranscoder, String streamName, String transcoderName,
				IApplicationInstance appInstance, LiveStreamTranscoderItem liveStreamTranscoderItem) {

		}

		@Override
		public void onInitStop(LiveStreamTranscoder liveStreamTranscoder) {
			
		}

		@Override
		public void onRegisterStreamNameGroup(LiveStreamTranscoder liveStreamTranscoder,
				TranscoderStreamNameGroup streamNameGroup) {
			
		}

		@Override
		public void onResetStream(LiveStreamTranscoder liveStreamTranscoder) {
			
		}

		@Override
		public void onSessionAudioDecodeCodecInfo(LiveStreamTranscoder liveStreamTranscoder,
				MediaCodecInfoAudio codecInfoAudio) {

		}

		@Override
		public void onSessionAudioEncodeCodecInfo(LiveStreamTranscoder liveStreamTranscoder,
				TranscoderSessionAudioEncode sessionAudioEncode, MediaCodecInfoAudio codecInfoAudio) {
			
		}

		@Override
		public void onSessionAudioEncodeCreate(LiveStreamTranscoder liveStreamTranscoder,
				TranscoderSessionAudioEncode sessionAudioEncode) {

		}

		@Override
		public void onSessionAudioEncodeInit(LiveStreamTranscoder liveStreamTranscoder,
				TranscoderSessionAudioEncode sessionAudioEncode) {
		}

		@Override
		public void onSessionAudioEncodeSetup(LiveStreamTranscoder liveStreamTranscoder,
				TranscoderSessionAudioEncode sessionAudioEncode) {
			
		}

		@Override
		public void onSessionDataEncodeCreate(LiveStreamTranscoder liveStreamTranscoder,
				TranscoderSessionDataEncode sessionDataEncode) {

		}

		@Override
		public void onSessionDataEncodeInit(LiveStreamTranscoder liveStreamTranscoder,
				TranscoderSessionDataEncode sessionDataEncode) {
		}

		@Override
		public void onSessionDestinationCreate(LiveStreamTranscoder liveStreamTranscoder,
				TranscoderSessionDestination sessionDestination) {

			
		}

		@Override
		public void onSessionVideoDecodeCodecInfo(LiveStreamTranscoder liveStreamTranscoder,
				MediaCodecInfoVideo codecInfoVideo) {
		}

		@Override
		public void onSessionVideoEncodeCodecInfo(LiveStreamTranscoder liveStreamTranscoder,
				TranscoderSessionVideoEncode sessionVideoEncode, MediaCodecInfoVideo codecInfoVideo) {
		}

		@Override
		public void onSessionVideoEncodeCreate(LiveStreamTranscoder liveStreamTranscoder,
				TranscoderSessionVideoEncode sessionVideoEncode) {
		}

		@Override
		public void onSessionVideoEncodeInit(LiveStreamTranscoder liveStreamTranscoder,
				TranscoderSessionVideoEncode sessionVideoEncode) {
		}

		@Override
		public void onSessionVideoEncodeSetup(LiveStreamTranscoder liveStreamTranscoder,
				TranscoderSessionVideoEncode sessionVideoEncode) {
		}

		@Override
		public void onShutdownStart(LiveStreamTranscoder liveStreamTranscoder) {
		}

		@Override
		public void onShutdownStop(LiveStreamTranscoder liveStreamTranscoder) {
		}

		@Override
		public void onUnregisterStreamNameGroup(LiveStreamTranscoder liveStreamTranscoder,
				TranscoderStreamNameGroup streamNameGroup) {
		}
		
	}

	/**
	 * Listener permtting to get the video codec information of each stream, in order to take the resolution from it
	 */
	class StreamListener implements IMediaStreamActionNotify3
	{
		
		@Override
		public void onCodecInfoVideo(IMediaStream stream,
				MediaCodecInfoVideo codecInfoVideo) {
			WMSProperties props = stream.getProperties();

			if (!props.containsKey("codecInfoVideoHeight") && ! props.containsKey("codecInfoVideoWidth")) {
				synchronized (props)
				{
					props.put("codecInfoVideoHeight", codecInfoVideo.getFrameHeight());
					props.put("codecInfoVideoWidth", codecInfoVideo.getFrameWidth());
				}
			}

			getLogger().debug("onCodecInfoVideo[" + stream.getName() + " Video Codec" + codecInfoVideo.toCodecsStr() + " ; " + codecInfoVideo.getFrameHeight() + "*" + codecInfoVideo.getFrameWidth() + "]: " + stream.getLiveStreamTranscoders().entrySet().size());
			checkStreamValidity(stream);
			
		}

		@Override
		public void onMetaData(IMediaStream stream, AMFPacket metaDataPacket)
		{

		}

		@Override
		public void onPauseRaw(IMediaStream stream, boolean isPause, double location)
		{
		}

		@Override
		public void onPause(IMediaStream stream, boolean isPause, double location)
		{
		}

		@Override
		public void onPlay(IMediaStream stream, String streamName, double playStart, double playLen, int playReset)
		{
		}

		@Override
		public void onPublish(IMediaStream stream, String streamName, boolean isRecord, boolean isAppend)
		{
		}

		@Override
		public void onSeek(IMediaStream stream, double location)
		{
		}

		@Override
		public void onStop(IMediaStream stream)
		{
		}

		@Override
		public void onUnPublish(IMediaStream stream, String streamName, boolean isRecord, boolean isAppend)
		{
		}

		@Override
		public void onCodecInfoAudio(IMediaStream stream,
				MediaCodecInfoAudio codecInfoAudio) {
		}

	}
	
	/**
	 * Check if the stream is a transcoded stream, and if it's the case, shutdown it if its resolution is better than
	 * the one of the source stream.
	 */
	private void checkStreamValidity(IMediaStream stream) {
		if (stream.isTranscodeResult()) {
			WMSProperties appProps = stream.getStreams().getAppInstance().getProperties();
			Set<ILiveStreamTranscoder> transcoders = (Set<ILiveStreamTranscoder>)(appProps.getProperty("transcoders"));
			if (transcoders != null) {
				for (ILiveStreamTranscoder transcoder : transcoders) {
					if (stream.getName().startsWith(transcoder.getStreamName() + "_")) {
						IMediaStream srcStream = stream.getStreams().getStream(transcoder.getStreamName());
						WMSProperties srcStreamProperties = srcStream.getProperties();
						int srcStreamWidth = srcStreamProperties.getPropertyInt("codecInfoVideoWidth", -1);
						int srcStreamHeight = srcStreamProperties.getPropertyInt("codecInfoVideoHeight", -1);
						WMSProperties streamProperties = stream.getProperties();
						int streamWidth = streamProperties.getPropertyInt("codecInfoVideoWidth", -1);
						int streamHeight = streamProperties.getPropertyInt("codecInfoVideoHeight", -1);
						getLogger().debug("Stream width : " + streamWidth + " ; " + "src Stream width : " + srcStreamWidth + " / Stream height : " + streamHeight + " ; src Stream height : " + srcStreamHeight + " ----------------------- ; " + stream.getName() + " ; tr : " + transcoder.getStreamName());
						if (streamWidth > srcStreamWidth && streamHeight > srcStreamHeight) {
							// The resolution of the transcoded stream is better than the original : we remove the stream
							stream.shutdown();
							stream.close();
						}
					}
				}
			}
		}
	}

	/**
	 * Register the video codec listener
	 */
	public void onStreamCreate(IMediaStream stream)
	{
		getLogger().debug("onStreamCreate["+stream+"]: clientId:" + stream.getClientId());
		IMediaStreamActionNotify3 actionNotify = new StreamListener();

		WMSProperties props = stream.getProperties();
		synchronized (props)
		{
			props.put("streamActionNotifier", actionNotify);
		}
		stream.addClientListener(actionNotify);
	}

	/**
	 * Unregister the video codec listener
	 */
	public void onStreamDestroy(IMediaStream stream)
	{
		getLogger().debug("onStreamDestroy["+stream+"]: clientId:" + stream.getClientId());

		IMediaStreamActionNotify3 actionNotify = null;
		WMSProperties props = stream.getProperties();
		synchronized (props)
		{
			actionNotify = (IMediaStreamActionNotify3) stream.getProperties().get("streamActionNotifier");
		}
		if (actionNotify != null)
		{
			stream.removeClientListener(actionNotify);
			getLogger().debug("removeClientListener: " + stream.getSrc());
		}
	}

	/**
	 * A transcoded stream shouldn't be transcoded itself
	 */
	class TranscoderControl implements ILiveStreamTranscoderControl
	{
		/*
		 * A transcoded stream shouldn't be transcoded itself
		 */
		@Override
		public boolean isLiveStreamTranscode(String transcoderName, IMediaStream stream)
		{
			getLogger().debug("transcode stream ?: " + stream.getName());
			// here is there you return true or false if you want stream transcoded
			if ( stream.isTranscodeResult() ) 
			{ 
				return false; 
			} else {
				return true;
			}
			
		}
	}
	
	class RestrictedMediaListProvider implements IMediaListProvider
	{
		public MediaList resolveMediaList(IMediaListReader mediaListReader, IMediaStream stream, String streamName)
		{

			MediaList mediaList = new MediaList();

			List<MediaListRendition> renditions = new ArrayList<MediaListRendition>();

			// Get the name of the stream without group name
			String mainStreamName = streamName;
			
			if (streamName.lastIndexOf("_") >= 0) {
				mainStreamName = streamName.substring(0, streamName.lastIndexOf("_"));
			}
				
			for (IMediaStream streamM : stream.getStreams().getStreams()) {
				if (streamM.isTranscodeResult() && streamM.isOpen()) {
					
					String mMaintStreamName = streamM.getName();
					if (mMaintStreamName.lastIndexOf("_") >= 0) {
						mMaintStreamName = mMaintStreamName.substring(0, mMaintStreamName.lastIndexOf("_"));
					}
					
					if (mainStreamName.equals(mMaintStreamName)) {
						
						WMSProperties props = streamM.getProperties();
	
						MediaListRendition rendition = new MediaListRendition();
	
						if (props.containsKey("codecInfoVideoHeight") && props.containsKey("codecInfoVideoWidth")) {
							
							rendition.setName("mp4:" + streamM.getName());
							rendition.setWidth((int)props.get("codecInfoVideoWidth"));
							rendition.setHeight((int)props.get("codecInfoVideoHeight"));
							
							String encodeName = streamM.getName().substring(streamM.getName().lastIndexOf('_') + 1);
							EncodeConfiguration encodeConfig = EncodeConfigurationBuilder.getInstance().getEncodeConfigurationByName(encodeName);
							rendition.setBitrateAudio((int)encodeConfig.getBitrateAudio());
							rendition.setBitrateVideo((int)encodeConfig.getBitrateVideo());
							
							renditions.add(rendition);
							
						}
					}

				}
			}
			
			// Order the renditions by descending resolution
			Collections.sort(renditions, new RenditionComparator());
			
			MediaListSegment segment = new MediaListSegment();
			mediaList.addSegment(segment);
			
			for (MediaListRendition rendition : renditions) {
				segment.addRendition(rendition);
			}

			return mediaList;
		}
	}
	
	/**
	 * Comparator to order the renditions (depending of the height)
	 * @author odifis
	 *
	 */
	private class RenditionComparator implements Comparator<MediaListRendition> {

		@Override
		public int compare(MediaListRendition o1, MediaListRendition o2) {
			if (o1.getName() != null && o2.getName() != null) {
				if (o1.getName() == null) {
					return 1000001;
				}
				if (o2.getName() == null) {
					return -1000001;
				}
				if (o1.getName().equals(o2.getName())) {
					return 0;
				}
				if (o1.getName().endsWith("_source") && !o2.getName().endsWith("_source")) {
					return -1000000;
				}
				if (o2.getName().endsWith("source") && !o1.getName().endsWith("source")) {
					return 1000000;
				}
				return o2.getHeight() - o1.getHeight();
			} else {
				return 0;
			}
		}

		
	}
	
	/**
	 * Contains the bitrate date about the string generated by the transcoder
	 * @author odifis
	 *
	 */
	private static class EncodeConfiguration {
		
		private String name;
		
		private String streamName;
		
		private long bitrateVideo;
		
		private long bitrateAudio;
		
		public String getName() {
			return name;
		}

		public long getBitrateVideo() {
			return bitrateVideo;
		}

		public long getBitrateAudio() {
			return bitrateAudio;
		}

		public String getStreamName() {
			return streamName;
		}

		public void setName(String name) {
			this.name = name;
		}

		public void setStreamName(String streamName) {
			this.streamName = streamName;
		}

		public void setBitrateVideo(long bitrateVideo) {
			this.bitrateVideo = bitrateVideo;
		}

		public void setBitrateAudio(long bitrateAudio) {
			this.bitrateAudio = bitrateAudio;
		}
		
	}
	
	/**
	 * Get in the Wowza configuration file the Configurations of the encodes defined for the transcoder
	 * @author odifis
	 *
	 */
	private static class EncodeConfigurationBuilder {
		
		private static EncodeConfigurationBuilder instance;
		
		private Set<EncodeConfiguration> encodeConfigs = new HashSet<EncodeConfiguration>();
		
		private EncodeConfigurationBuilder() {
			
		}
		
		public static EncodeConfigurationBuilder getInstance() {
			if (instance == null) {
				instance = new EncodeConfigurationBuilder();
			}
			return instance;
		}
		
		public EncodeConfiguration addEncodeConfiguration(Node encodeNode) {
			EncodeConfiguration encodeConfig = new EncodeConfiguration();
			
			encodeConfig.setName(getFirstChildNodeByName(encodeNode, "Name").getTextContent());
			encodeConfig.setStreamName(getFirstChildNodeByName(encodeNode, "StreamName").getTextContent());
			
			Node videoBitrateNode = getFirstChildNodeByName(getFirstChildNodeByName(encodeNode, "Video"), "Bitrate");
			if (videoBitrateNode != null && videoBitrateNode.getTextContent() != null &&  !videoBitrateNode.getTextContent().isEmpty() && isLong(videoBitrateNode.getTextContent())) {
				encodeConfig.setBitrateVideo(Long.parseLong(videoBitrateNode.getTextContent()));
			}
			
			Node audioBitrateNode = getFirstChildNodeByName(getFirstChildNodeByName(encodeNode, "Audio"), "Bitrate");
			if (audioBitrateNode != null && audioBitrateNode.getTextContent() != null && !audioBitrateNode.getTextContent().isEmpty() && isLong(audioBitrateNode.getTextContent())) {
				encodeConfig.setBitrateAudio(Long.parseLong(audioBitrateNode.getTextContent()));
			}
			
			encodeConfigs.add(encodeConfig);
			return encodeConfig;
		}
		
		private boolean isLong(String s) {
		    try { 
		        Long.parseLong(s); 
		    } catch(NumberFormatException e) { 
		        return false; 
		    } catch(NullPointerException e) {
		        return false;
		    }
		    return true;
		}
		
		public EncodeConfiguration getEncodeConfigurationByName(String encodeName) {
			for (EncodeConfiguration encodeConfig : encodeConfigs) {
				if (encodeConfig.getName().equals(encodeName)) {
					return encodeConfig;
				}
			}
			return null;
		}
		
	}

	/**
	 * Get the first child node of the given name
	 * @param node
	 * @param nodeName
	 * @return
	 */
	private static Node getFirstChildNodeByName(Node node, String nodeName) {
		NodeList nodes = node.getChildNodes();
		for (int j=0; j < nodes.getLength(); j++) {
			Node nodeT = nodes.item(j);
			if (nodeT.getNodeName().equals(nodeName)) {
				return nodeT;
			}
		}
		return null;
	}
	
	/**
	 * Register the listeners of the transcoderControl and the transcoderListener
	 */
	public void onAppStart(IApplicationInstance appInstance)
	{
		String fullname = appInstance.getApplication().getName() + "/" + appInstance.getName();
		getLogger().info("onAppStart " + ModuleTranscoderControlByStream.class.getName() + " : " + fullname);
		appInstance.setLiveStreamTranscoderControl(new TranscoderControl());
		appInstance.addLiveStreamTranscoderListener(new TranscoderListener());
		appInstance.setMediaListProvider(new RestrictedMediaListProvider());
		
		// Get the possible bitrates depending of the resolution
		// Get the path of the transcoder template file in the Application.xml file
		// And get the configuration of the transcoder encodes in the transcoder definition xml file
		try {
			Document configApp = VHostConfig.getDocumentFromFile(VHostConfig.wowzaBasePath + "conf/" + appInstance.getApplication().getName() + "/Application.xml");
			NodeList transcoderList = configApp.getElementsByTagName("Transcoder");
			if (transcoderList.getLength() > 0 ) {
				Node transcoderNode = transcoderList.item(0);
				Node templateDirNode = getFirstChildNodeByName(transcoderNode, "TemplateDir");
				Node templatesNode = getFirstChildNodeByName(transcoderNode, "Templates");
				if (templateDirNode != null && templatesNode != null) {
					String directory = templateDirNode.getTextContent();
					String fileName = templatesNode.getTextContent();
					if (fileName.indexOf(',') >= 0) {
							fileName = fileName.substring(fileName.lastIndexOf(',')+1);
					}
					directory = directory.replace("{com.wowza.wms.context.VHostConfigHome}", VHostConfig.wowzaBasePath).replace("$", "");
					Document configTrans = VHostConfig.getDocumentFromFile(directory + "/" + fileName);
					NodeList nodes = configTrans.getElementsByTagName("Encode");
					for (int i = 0; i < nodes.getLength(); i++) {
						Node encode = nodes.item(i);
						EncodeConfigurationBuilder.getInstance().addEncodeConfiguration(encode);
					}
				}
				
			}
		} catch (Exception ex) {
			getLogger().error(ex.getMessage(), ex);
		}

	}
	
}
